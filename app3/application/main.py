
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 06:52:54 2021
app web
@author: nico
"""

from flask import Flask, flash, redirect, render_template, request, session, abort

#configure the app
app = Flask(__name__)

#main page
@app.route("/")
def index():
    words = "app3"
    return render_template('index.html', title="welcome", app=words)
    
# #contact page
# @app.route('/contact')
# def contact():
#     mail = "john@doe.com"
#     tel = "01 02 03 04 05"
#     return "mail: "+mail+" --- phone: "+tel

# #member page
# @app.route("/members")
# def members():
#     name = "john doe"
#     return name

# #member name page
# @app.route("/members/<string:name>/")
# def getMember(name):
#     return name

#run the app
# app.run(host='172.17.0.2',port=5000)
# app.run(host='127.0.0.1',port=5000)
app.run(host='0.0.0.0',port=5000)
# web.py
# Affichage de 23-web.py